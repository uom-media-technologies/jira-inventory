import conf
import requests
import json
import fcntl, socket, struct
import os
import subprocess
import uuid
import hashlib

import getRamInfo
import getHDDInfo
import getCPUInfo
import getMBInfo

JIRA_PROJECT = "IAM"
# MAC address url
url = 'https://{}/rest/api/2/search?jql=project={} AND "Hardware Property Labels"={}'
# MB serial number url
mb_url = 'https://{}/rest/api/2/search?jql=project={} AND "Serial Number"~{}'
url2 = 'https://{}/rest/api/2/search?jql=Issuekey={}'
h = None
the_key = None
ca_name = None
mb_result = None
issues_ticket_list = []
staging_status_id = 31
production_status_id = 21
storage_status_id = 11

def link_issue_to_agent(child, parent):
    linkjson = {
    "type": {
        "name": "Location",
    },
    "inwardIssue": {
        "key": child
    },
    "outwardIssue": {
        "key": parent
    }
}

    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issueLink/'.format(conf.jira_url())
    headers = {'Content-Type': 'application/json'}
    r = requests.post(url, auth=(conf.jira_user(), conf.jira_pass()), data=json.dumps(linkjson), headers=headers)
    return r


def delete_existing_link(linkid):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issueLink/{}'.format(conf.jira_url(), linkid)
    headers = {'Content-Type': 'application/json'}
    r = requests.delete(url, auth=(conf.jira_user(), conf.jira_pass()), headers=headers)
    return r


def get_existing_link(linkid):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issueLink/{}'.format(conf.jira_url(), linkid)
    headers = {'Content-Type': 'application/json'}
    r = requests.get(url, auth=(conf.jira_user(), conf.jira_pass()), headers=headers)
    return r


def get_meta():
    # NOT USED . for debugging JIRA permissions to edit only!
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/{}/editmeta'.format(conf.jira_url(), 'CAC-54')
    headers = {'Content-Type': 'application/json'}
    r = requests.get(url, auth=(conf.jira_user(), conf.jira_pass()), headers=headers)
    print r.content


def add_new_inventory(issue_json):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/'.format(conf.jira_url())
    headers = {'Content-Type': 'application/json'}

    r = requests.post(url, auth=(conf.jira_user(), conf.jira_pass()), data=json.dumps(issue_json), headers=headers)
    print 'added a new inventory item with issue ID: ' + r.json()['key']
    issues_ticket_list.append(r.json()['key'])
    return r


def update_inventory(issue_json, issue_id):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/{}'.format(conf.jira_url(), issue_id)
    headers = {'Content-Type': 'application/json', "X-Atlassian-Token": "nocheck"}

    r = requests.put(url, auth=(conf.jira_user(), conf.jira_pass()), data=json.dumps(issue_json), headers=headers)
    return r


def get_inventory_issue(issue_id):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/{}'.format(conf.jira_url(), issue_id)
    headers = {'Content-Type': 'application/json', "X-Atlassian-Token": "nocheck"}

    r = requests.get(url, auth=(conf.jira_user(), conf.jira_pass()), headers=headers)
    return r


def get_workflow(issue_id):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/{}?fields=status'.format(conf.jira_url(), issue_id)
    headers = {'Content-Type': 'application/json'}

    r = requests.get(url, auth=(conf.jira_user(), conf.jira_pass()), headers=headers)
    return r


def update_workflow(issue_json, issue_id):
    # Post the new issue to jira
    url = 'https://{}/rest/api/2/issue/{}/transitions'.format(conf.jira_url(), issue_id)
    headers = {'Content-Type': 'application/json'}

    r = requests.post(url, auth=(conf.jira_user(), conf.jira_pass()), data=json.dumps(issue_json), headers=headers)
    return r


def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ':'.join(['%02x' % ord(char) for char in info[18:24]])


def getHwlist():
    # get a list of all the network interfaces except for lo
    hwlist = []
    interfaces = os.listdir('/sys/class/net/')
    # print interfaces
    for i in interfaces:
        if i != 'lo':
            try:
                hwadd = getHwAddr(i)
            except IOError:
                # print 'no device'
                hwadd = None
            hwlist.append(hwadd)
    return hwlist


# def getMotherBoardSerial():
#     mb_ser = subprocess.check_output(['dmidecode', '-s', 'baseboard-serial-number']) #'"/6DZW5Y1/CN7220034F05AD/"' #'E5A28200397'
#     return mb_ser.strip()


def get_issue_id(results):
    # returns the issue ID of the capture agent from that of the motherboard information
    for k in results.json()['issues']:
        for l in k['fields']['issuelinks']:
            return l['outwardIssue']['key']


def get_agent_issue_by_mb():

    try:
        mb_serial = getMBInfo.get_mb_serial()
        # lookup mb serial in JIRA
        mb_result = requests.get(mb_url.format(conf.jira_url(), conf.jira_project(), "'{}'".format(mb_serial)),
                                 auth=(conf.jira_user(), conf.jira_pass()))
    except Exception as e:
        print e
    if mb_result.json()['total'] == 0:
        print 'no results for motherboard lookup'
        return None
    # lookup ca_name if Motherboard serial matches
    else:
        print 'found issue via motherboard serial'
        return mb_result


def get_agent_issue_by_mac():
    # try to find the Capture Agent issue linked to the motherboard MAC address
    mac_addresses = getHwlist()
    # print mac_addresses
    for mac in mac_addresses:
        try:
            h = requests.get(url.format(conf.jira_url(), conf.jira_project(), mac.replace(':', '').upper()), auth=(conf.jira_user(), conf.jira_pass()))
            # print h.json()
            if h.json()['total'] >= 1:
                break
        except Exception as e:
            print e
    # print h.json()['total']
    # if cant find a machine with the mac in jira try motherboard
    if h.json()['total'] == 0:
        print 'no results for MAC address lookup'
        return None
    # lookup ca_name if MAC found
    else:
        print 'found via mac address lookup'
        return h


def get_agent_issue():
    print 'looking up current agent issue ID'
    # try getting the parent captre agent issue by mac, if that returns nothing try motherboard serial number
    issue_jira_mc = get_agent_issue_by_mac()
    if issue_jira_mc:
        return get_issue_id(issue_jira_mc)
    else:
        issue_jira_mb = get_agent_issue_by_mb()
        if issue_jira_mb:
            return get_issue_id(issue_jira_mb)

def add_new_capture_agent():
    # set hardware vars
    # agent serial is 'Capture-Agent-{7digituuid}'
    ca_uuid = str(uuid.uuid4())[:7]
    issue_summary = "Capture-Agent-" + ca_uuid
    agent_serial = ca_uuid
    # get a location from gc config

    fields = {
        "fields": {
            "project":
                {
                    "key": JIRA_PROJECT
                },
            "summary": issue_summary,
            "customfield_10600": agent_serial,
            "customfield_10705": {"value": conf.get_ca_name()},
                "issuetype": {
                "name": "Capture Agent"
            }
        }
    }
    print 'adding new ' + issue_summary
    # add a new ca
    ca_id = add_new_inventory(fields).json()["key"]
    # then create and append a motherboard
    add_new_motherboard(ca_id)
    return ca_id

def add_new_motherboard(ca_id):
    # set hardware vars
    issue_summary = getMBInfo.get_mb_model()
    mb_serial = getMBInfo.get_mb_serial()
    mb_manu = getMBInfo.get_mb_man()

    # get a location from gc config

    fields = {
        "fields": {
            "project":
                {
                    "key": JIRA_PROJECT
                },
            "summary": issue_summary,
            "customfield_10600": mb_serial,
            "customfield_10612": [mb_manu],
            "customfield_10606": [issue_summary],
                "issuetype": {
                "name": "Mother Board"
            }
        }
    }
    print 'adding new ' + issue_summary
    new_issue_id = add_new_inventory(fields).json()["key"]
    link_issue_to_agent(new_issue_id, ca_id)


def confirm_new_inventory(hw_vars, hw_serial, hwtype):
    # get the current issue id of the capture agent this hardware is in
    current_agent_id = get_agent_issue()
    # FIXME to check if current_agent_id exists, if not create a new agent and mb
    if current_agent_id == None:
        # create new agent (assumption: must at least have a mac or some type of serial against the agent issue ticket
        current_agent_id = add_new_capture_agent()

    # Check if this hardware already exists from serial number
    hw_result = requests.get(mb_url.format(conf.jira_url(), conf.jira_project(), "'{}'".format(hw_serial)),
                             auth=(conf.jira_user(), conf.jira_pass()))
    if hw_result.json()['total'] == 0:
        # does not exist so create and link to agent
        print 'hardware not in jira inventory'
        print 'adding new hardware of type {}'.format(hwtype)
        # add new issue
        new_issue_id = add_new_inventory(hw_vars).json()["key"]
        link_issue_to_agent(new_issue_id, current_agent_id)
    else:
        # if exists is it in a new capture agent?
        jira_agent_id = get_issue_id(hw_result)
        issues_ticket_list.append(jira_agent_id)
        print 'already exists in JIRA inventory'
        # if the current capture agent does not equal the one in jira, update it
        # CPU's dont have uuid's so dont move it!
        if jira_agent_id != current_agent_id and hwtype != "CPU":
            # gets the current link id
            existing_link_id = hw_result.json()['issues'][0]['fields']['issuelinks'][0]['id']
            # deletes the current link
            delete_existing_link(existing_link_id)
            # update to link new agent id
            link_issue_to_agent(hw_result.json()['issues'][0]['key'], current_agent_id)
        else:
            issues_ticket_list.append(hw_result.json()['issues'][0]['key'])
            print 'hardware item still in current capture agent or its a CPU, not updating'


def add_ram():
    # set hardware vars
    for ser, dimm in getRamInfo.get_all_ram_info().iteritems():
        summary = 'RAM'
        serial = ser
        try:
            manufacturer = dimm['Manufacturer']
        except KeyError:
            manufacturer = 'Unknown'
        try:
            size = dimm['Size']
        except KeyError:
            size = 'Unknown'
        try:
            speed = dimm['Speed']
        except KeyError:
            speed = 'Unknown'
        try:
            asset = dimm['Asset Tag']
        except KeyError:
            asset = 'Unknown'
        try:
            part = dimm['Part Number']
        except KeyError:
            part = 'Unknown'

        fields = {
            "fields": {
                "project":
                    {
                        "key": JIRA_PROJECT
                    },
                "summary": summary,
                "customfield_10600": serial,
                "customfield_10612": [manufacturer],
                "customfield_10703": 4,
                "customfield_10704": [speed[0:4]],
                "customfield_10609": asset + ' ' + part,
                    "issuetype": {
                    "name": summary
                }
            }
        }
        print 'adding new RAM'
        confirm_new_inventory(fields, serial, summary)

def add_disk():
    # set hardware vars
    for ser, dimm in getHDDInfo.get_all_hdd_info().iteritems():
        serial = ser
        try:
            manufacturer = dimm['Model Number']
        except KeyError:
            manufacturer = 'Unknown'
        try:
            size = dimm['device size with M = 1000*1000']
            size = size[size.find("(")+1:size.find(")")]
            size = size.split()[0]
        except KeyError:
            size = 'Unknown'
        try:
            formfac = dimm['Speed']
        except KeyError:
            formfac = 'Unknown'
        try:
            firmw = dimm['Firmware Revision']
        except KeyError:
            firmw = 'Unknown'
        brand = manufacturer.split()[0]
        model = ''.join(manufacturer.split()[1:])
        fields = {
            "fields": {
                "project":
                    {
                        "key": JIRA_PROJECT
                    },
                "summary": manufacturer,
                "customfield_10600": serial,
                "customfield_10612": [brand],
                "customfield_10606": [model],
                "customfield_10701": int(size),
                "customfield_10609": firmw,
                    "issuetype": {
                    "name": 'Disk Drive'
                }
            }
        }
        print 'adding new Disk Drive'
        confirm_new_inventory(fields, serial, 'Disk Drive')


def add_cpu():
    # set hardware vars
    # Need to create a CPU serial as they dont give one
    issue_summary = "CPU"
    cpu_serial = getCPUInfo.get_cpu_serial()
    cpu_man = getCPUInfo.get_cpu_man()
    cpu_model = getCPUInfo.get_cpu_model()
    mac = getHwlist()[0]
    # make up a serial for the thing
    cpu_made_serial = hashlib.md5(cpu_serial + mac + cpu_model).hexdigest()
    cpu_family = getCPUInfo.get_cpu_family()
    fields = {
        "fields": {
            "project":
                {
                    "key": JIRA_PROJECT
                },
            "summary": issue_summary + '-' + cpu_family,
            "customfield_10600": cpu_made_serial,
            "customfield_10612": [cpu_man],
            "customfield_10606": [cpu_model],
                "issuetype": {
                "name": issue_summary
            }
        }
    }
    print 'adding new ' + issue_summary
    confirm_new_inventory(fields, cpu_made_serial, issue_summary)


def update_motherboard():
    # FIXME this function needs more work should do lookup not on mb serial but from main ticket --> motherbord ticket
    # setting vars for motherboard issue
    if get_agent_issue_by_mac():
        print 'mac addresses are already in JIRA, not updating'
    else:
        print 'attempting to update mb issue with mac addresses'

        # get current mac addresses
        macs_new = []
        for mac in getHwlist():
            macs_new.append(mac.replace(':', '').upper())
        fields = {
            "fields" : {
                "customfield_10610" : macs_new
            }
        }
        mb_issue = get_agent_issue_by_mb()
        mb_issue_fmt = mb_issue.json()['issues'][0]['key']
        issues_ticket_list.append(mb_issue_fmt)
        update_inventory(fields, mb_issue_fmt)
        print 'updated ' + mb_issue_fmt + ' with mac addresses.'

def update_ca_location():
    # If location is changed update the ca ticket in jira with the new location
    issue_id = get_agent_issue()
    current_location = get_inventory_issue(issue_id).json()['fields']['customfield_10705']['value']
    new_location = conf.get_ca_name()
    if current_location != new_location:
        fields = {
            "fields": {
                "customfield_10705": {"value": new_location}
            }
        }
        update_inventory(fields, issue_id)


def set_workflow():
    print 'checking current workflow'
    new_status_id = 11
    new_status_name = 'In Storage'
    new_status = conf.jira_status()
    capture_agent = get_agent_issue()
    current_workflow = get_workflow(capture_agent)
    if new_status == 'staging':
        new_status_id = staging_status_id
        new_status_name = 'In Staging'
    elif new_status == 'production':
        new_status_id = production_status_id
        new_status_name = 'In Production'
    else:
        if current_workflow.json()['fields']['status']['name'] == 'Open':
            new_status_id = storage_status_id
            new_status_name = 'In Storage'
    current_status_name = current_workflow.json()['fields']['status']['name']
    if current_status_name != new_status_name:
        fields = {
            "transition": {
                "id": new_status_id
            }
        }
        print 'setting workflow as: {}'.format(new_status_id)

        update_workflow(fields, capture_agent)


print 'Getting each hardware component, checking JIRA for inventory items and creating new inventory if it ' \
      'does not exist'

add_ram()
add_disk()
add_cpu()
update_motherboard()
set_workflow()
update_ca_location()

print 'Writing issue IDs to file'
all_issues_frmt = ','.join(list(set(issues_ticket_list)))
f = open('/var/www/JIRA_inventory', 'w')
f.write(all_issues_frmt)
f.close()
