import subprocess

no_value = "To be filled by O.E.M."


def get_mb_serial():
    mb_ser = subprocess.check_output(['dmidecode', '-s', 'baseboard-serial-number'])  # '"/6DZW5Y1/CN7220034F05AD/"' #'E5A28200397'
    mb_system_ser = subprocess.check_output(['dmidecode', '-s', 'system-serial-number'])
    if mb_ser.strip() != no_value:
        return mb_ser.strip()
    elif mb_system_ser.strip() != no_value:
        return mb_system_ser.strip()
    else:
        mb_ser = subprocess.check_output(['dmidecode', '-s', 'system-uuid'])
        if mb_ser.strip() != no_value:
            return mb_ser.strip()


def get_mb_man():

    mb_manufacturer = subprocess.check_output(['dmidecode', '-s', 'baseboard-manufacturer'])
    return mb_manufacturer.replace(' ', '')


def get_mb_model():

    mb_model = subprocess.check_output(['dmidecode', '-s', 'baseboard-product-name'])
    return mb_model.replace(' ', '')


def get_mb_asset():

    mb_asset = subprocess.check_output(['dmidecode', '-s', 'baseboard-asset-tag'])
    return mb_asset.replace(' ', '-')