import subprocess



def get_cpu_serial():
    # NOTE this isnt a real serial number, seems CPU's dont send that info
    cpu_serial = subprocess.check_output(['dmidecode', '--type', 'processor'])

    # separate each dimm
    for dimms in cpu_serial.replace('\t', '').split('\n\n'):
        # separate all the params
        for i in dimms.split('\n'):
            # get serial
            if i.startswith('ID:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    serial = i.split(': ')[1].strip()

                    return serial.replace(' ', '')


def get_cpu_man():

    cpu_manufacturer = subprocess.check_output(['dmidecode', '-s', 'processor-manufacturer'])
    return cpu_manufacturer.replace(' ', '')


def get_cpu_model():

    cpu_model = subprocess.check_output(['dmidecode', '-s', 'processor-version'])
    return cpu_model.replace(' ', '')

def get_cpu_family():
    # brings back 'i7' for come weird reason
    # cpu_family= subprocess.check_output(['dmidecode', '-s', 'processor-family'])
    cpu_family = subprocess.check_output(['dmidecode', '-s', 'processor-version'])
    cpu_family_fmt = cpu_family.split()[2]
    return cpu_family_fmt.replace(' ', '-')