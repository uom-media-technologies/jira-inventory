import subprocess
import stat
import os

test_data = "\n/dev/sda:\n\nATA device, with non-removable media\n\tModel Number:       Corsair Force LS SSD                    \n\tSerial Number:      17247940000122660034\n\tFirmware Revision:  SBFM01.0\n\tTransport:          Serial, ATA8-AST, SATA 1.0a, SATA II Extensions, SATA Rev 2.5, SATA Rev 2.6, SATA Rev 3.0\nStandards:\n\tSupported: 11 10 9 8 7 6 5 \n\tLikely used: 11\nConfiguration:\n\tLogical\t\tmax\tcurrent\n\tcylinders\t16383\t16383\n\theads\t\t16\t16\n\tsectors/track\t63\t63\n\t--\n\tCHS current addressable sectors:   16514064\n\tLBA    user addressable sectors:  234441648\n\tLBA48  user addressable sectors:  234441648\n\tLogical  Sector size:                   512 bytes\n\tPhysical Sector size:                   512 bytes\n\tLogical Sector-0 offset:                  0 bytes\n\tdevice size with M = 1024*1024:      114473 MBytes\n\tdevice size with M = 1000*1000:      120034 MBytes (120 GB)\n\tcache/buffer size  = unknown\n\tForm Factor: 2.5 inch\n\tNominal Media Rotation Rate: Solid State Device\nCapabilities:\n\tLBA, IORDY(can be disabled)\n\tQueue depth: 32\n\tStandby timer values: spec'd by Standard, no device specific minimum\n\tR/W multiple sector transfer: Max = 16\tCurrent = 16\n\tDMA: mdma0 mdma1 mdma2 udma0 udma1 udma2 udma3 udma4 udma5 *udma6 \n\t     Cycle time: min=120ns recommended=120ns\n\tPIO: pio0 pio1 pio2 pio3 pio4 \n\t     Cycle time: no flow control=120ns  IORDY flow control=120ns\nCommands/features:\n\tEnabled\tSupported:\n\t   *\tSMART feature set\n\t    \tSecurity Mode feature set\n\t   *\tPower Management feature set\n\t   *\tWrite cache\n\t   *\tLook-ahead\n\t   *\tHost Protected Area feature set\n\t   *\tWRITE_BUFFER command\n\t   *\tREAD_BUFFER command\n\t   *\tNOP cmd\n\t   *\tDOWNLOAD_MICROCODE\n\t    \tSET_MAX security extension\n\t   *\t48-bit Address feature set\n\t   *\tDevice Configuration Overlay feature set\n\t   *\tMandatory FLUSH_CACHE\n\t   *\tFLUSH_CACHE_EXT\n\t   *\tSMART error logging\n\t   *\tSMART self-test\n\t   *\tGeneral Purpose Logging feature set\n\t   *\tWRITE_{DMA|MULTIPLE}_FUA_EXT\n\t   *\t64-bit World wide name\n\t   *\tWRITE_UNCORRECTABLE_EXT command\n\t   *\t{READ,WRITE}_DMA_EXT_GPL commands\n\t   *\tSegmented DOWNLOAD_MICROCODE\n\t   *\tGen1 signaling speed (1.5Gb/s)\n\t   *\tGen2 signaling speed (3.0Gb/s)\n\t   *\tGen3 signaling speed (6.0Gb/s)\n\t   *\tNative Command Queueing (NCQ)\n\t   *\tPhy event counters\n\t   *\tREAD_LOG_DMA_EXT equivalent to READ_LOG_EXT\n\t   *\tDMA Setup Auto-Activate optimization\n\t    \tDevice-initiated interface power management\n\t   *\tSoftware settings preservation\n\t   *\tDOWNLOAD MICROCODE DMA command\n\t   *\tSET MAX SETPASSWORD/UNLOCK DMA commands\n\t   *\tWRITE BUFFER DMA command\n\t   *\tREAD BUFFER DMA command\n\t   *\tDEVICE CONFIGURATION SET/IDENTIFY DMA commands\n\t   *\tData Set Management TRIM supported (limit 8 blocks)\nSecurity: \n\tMaster password revision code = 65534\n\t\tsupported\n\tnot\tenabled\n\tnot\tlocked\n\t\tfrozen\n\tnot\texpired: security count\n\t\tsupported: enhanced erase\n\t20min for SECURITY ERASE UNIT. 60min for ENHANCED SECURITY ERASE UNIT. \nLogical Unit WWN Device Identifier: 5000000000000000\n\tNAA\t\t: 5\n\tIEEE OUI\t: 000000\n\tUnique ID\t: 000000000\nChecksum: correct\n"


def disk_exists(path):
     try:
             return stat.S_ISBLK(os.stat(path).st_mode)
     except:
             return False


def get_all_hdd_info():
    jeb = {}
    # FIXME ignore getting the HDD for nvme drives as this is hard right now
    if disk_exists('/dev/nvme0n1'):
        return jeb
    mb_ser = subprocess.check_output(['hdparm', '-I', '/dev/sda'])

    for dimms in mb_ser.replace('\t', '').split('\n\n'):
        cabs = {}

        # separate all the params
        for i in dimms.split('\n'):
            # get serial
            if i.startswith('Serial Number:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    serial = i.split(': ')[1].strip()

                    jeb[serial] = cabs
                    # get the rest if it at least has valid a serial
                    # get manufacturer
            if i.startswith('Model Number:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    manu = i.split(': ')[1].strip()
                    cabs[i.split(': ')[0]] = i.split(': ')[1].strip()
                    # get size
            if i.startswith('device size with M = 1000*1000:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    ram_size = i.split(': ')[1].strip()
                    cabs[i.split(': ')[0]] = i.split(': ')[1].strip()
            # get speed
            if i.startswith('Speed:'):
                if i.split(': ')[1][0:3].isdigit() and i.split(': ')[1] != 'Unknown':
                    ram_speed = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
            # get type
            if i.startswith('Form Factor:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    ram_type = i.split(': ')[1].strip()
                    cabs[i.split(': ')[0]] = i.split(': ')[1].strip()
            # get asset
            if i.startswith('Firmware Revision:'):
                if i.split(': ')[1] and i.split(': ')[1] != 'Unknown':
                    ram_asset = i.split(': ')[1].strip()
                    cabs[i.split(': ')[0]] = i.split(': ')[1].strip()
            # get part
            if i.startswith('Part Number:'):
                if i.split(': ')[1][0:3].isalnum() and i.split(': ')[1] != 'Unknown':
                    ram_part = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
    return jeb
