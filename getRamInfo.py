import subprocess

test_ram_info = ['# dmidecode 3.0\nGetting SMBIOS data from sysfs.\nSMBIOS 2.7 present.', 'Handle 0x0042, DMI type 16, 23 bytes\nPhysical Memory Array\nLocation: System Board Or Motherboard\nUse: System Memory\nError Correction Type: None\nMaximum Capacity: 32 GB\nError Information Handle: Not Provided\nNumber Of Devices: 2', 'Handle 0x0043, DMI type 17, 34 bytes\nMemory Device\nArray Handle: 0x0042\nError Information Handle: Not Provided\nTotal Width: Unknown\nData Width: Unknown\nSize: No Module Installed\nForm Factor: DIMM\nSet: None\nLocator: ChannelA-DIMM0\nBank Locator: BANK 0\nType: Unknown\nType Detail: None\nSpeed: Unknown\nManufacturer: [Empty]\nSerial Number: [Empty]\nAsset Tag: Not Specified\nPart Number: [Empty]\nRank: Unknown\nConfigured Clock Speed: Unknown', 'Handle 0x0044, DMI type 17, 34 bytes\nMemory Device\nArray Handle: 0x0042\nError Information Handle: Not Provided\nTotal Width: Unknown\nData Width: Unknown\nSize: No Module Installed\nForm Factor: DIMM\nSet: None\nLocator: ChannelA-DIMM1\nBank Locator: BANK 1\nType: Unknown\nType Detail: None\nSpeed: Unknown\nManufacturer: [Empty]\nSerial Number: [Empty]\nAsset Tag: Not Specified\nPart Number: [Empty]\nRank: Unknown\nConfigured Clock Speed: Unknown', 'Handle 0x0045, DMI type 17, 34 bytes\nMemory Device\nArray Handle: 0x0042\nError Information Handle: Not Provided\nTotal Width: 64 bits\nData Width: 64 bits\nSize: 4096 MB\nForm Factor: DIMM\nSet: None\nLocator: ChannelB-DIMM0\nBank Locator: BANK 2\nType: DDR3\nType Detail: Synchronous\nSpeed: 1600 MHz\nManufacturer: Kingston\nSerial Number: 390EE7B5\nAsset Tag: 9876543210\nPart Number: 9905584-017.A00LF \nRank: 1\nConfigured Clock Speed: 1600 MHz', 'Handle 0x0046, DMI type 17, 34 bytes\nMemory Device\nArray Handle: 0x0042\nError Information Handle: Not Provided\nTotal Width: Unknown\nData Width: Unknown\nSize: No Module Installed\nForm Factor: DIMM\nSet: None\nLocator: ChannelB-DIMM1\nBank Locator: BANK 3\nType: Unknown\nType Detail: None\nSpeed: 1500\nManufacturer: [Empty]\nSerial Number: 55466\nAsset Tag: Not Specified\nPart Number: [Empty]\nRank: Unknown\nConfigured Clock Speed: Unknown', '']





def get_all_ram_info():
    jeb = {}

    mb_ser = subprocess.check_output(['dmidecode', '--type', 'memory'])

    # separate each dimm
    for dimms in mb_ser.replace('\t', '').split('\n\n'):
        cabs = {}
        # separate all the params
        for i in dimms.split('\n'):
            # get serial
            if i.startswith('Serial Number:'):
                if i.split(': ')[1].isalnum() and i.split(': ')[1] != 'Unknown':
                    serial = i.split(': ')[1]

                    jeb[serial] = cabs
                    # get the rest if it at least has valid a serial
                    # get manufacturer
            if i.startswith('Manufacturer:'):
                if i.split(': ')[1].isalnum() and i.split(': ')[1] != 'Unknown':
                    manu = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
                # get size
            if i.startswith('Size:'):
                if i.split(': ')[1][0:3].isdigit() and i.split(': ')[1] != 'Unknown':
                    ram_size = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
            # get speed
            if i.startswith('Speed:'):
                if i.split(': ')[1][0:3].isdigit() and i.split(': ')[1] != 'Unknown':
                    ram_speed = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
            # get type
            if i.startswith('Type:'):
                if i.split(': ')[1].isalnum() and i.split(': ')[1] != 'Unknown':
                    ram_type = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
            # get asset
            if i.startswith('Asset Tag:'):
                if i.split(': ')[1].isalnum() and i.split(': ')[1] != 'Unknown':
                    ram_asset = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
            # get part
            if i.startswith('Part Number:'):
                if i.split(': ')[1][0:3].isalnum() and i.split(': ')[1] != 'Unknown':
                    ram_part = i.split(': ')[1]
                    cabs[i.split(': ')[0]] = i.split(': ')[1]
    return jeb
